cmake_minimum_required(VERSION 3.8)
project(try_2)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp tritset.cpp)
add_executable(try_2 ${SOURCE_FILES})