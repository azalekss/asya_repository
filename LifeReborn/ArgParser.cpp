#include "ArgParser.h"


// =================== OPTION RULE SET =====================


// Добававляем опции в options
void OptionRuleSet::addOption(Option *option) {
    forms["-" + option->shortForm] = option;
    forms["--" + option->longForm] = option;
    options.push_back(option);
}

// Выводим help
std::string OptionRuleSet::getHelp() {
    std::string s = this->programName + std::string("\n")
                    + this->about + "\n\n";

    for (auto &i : options) {
        s += i->getHint() + "\n\n";
    }

    return s;
}

std::map<std::string, OptionValues*> OptionRuleSet::parse(CommandLineIterator begin, CommandLineIterator end) {
    auto it = begin;
    std::map<std::string, OptionValues*> ovs;

    while (it != end) {
        std::string key = *it;
        Option &option = *forms[key];

        if (&option == nullptr) {
            throw std::invalid_argument("Failed to parse command line arguments: use --help or -h to get help");
        }

        it++;

        auto ov = &option.parse(it, end);
        ov->optionName = option.name;
        ovs[ov->optionName] = ov;
    }
    return ovs;
}

OptionRuleSet::OptionRuleSet() {
    forms = std::map<std::string, Option *>();
    options = std::vector<Option *>();
}

// =================== OPTION =====================

// Конструктор Option
Option::Option(std::string name, std::string longForm, std::string shortForm) {
    this->name = name;
    this->longForm = longForm;
    this->shortForm = shortForm;
}

// Добавляем аргумент в args
void Option::addArgument(ArgumentType *arg) {
    args.push_back(arg);
}

//Даём информацию по Option-у
std::string Option::getHint() {
    std::string s = "--" + this->longForm + ", "
                    + "-" + this->shortForm + " ";

    for (auto &i : args) {
        s += i->getHint() + " ";
    }
    s += ":\n";
    s += this->about;

    return s;
}

OptionValues &Option::parse(CommandLineIterator &it, CommandLineIterator end) {
    OptionValues *op = new OptionValues(name);

    for (auto &i : args) {
        if (it == end) {
            throw std::invalid_argument("Too less arguments");
        }
        op->values.push_back(&i->parse(*it));
        it++;
    }

    return *op;
}

// =================== ARGUMENT TYPE =====================


Value & ArgumentType::parse(std::string) {
    std::cout << "I'm AT";
    return *new Value(10);
}

std::string ArgumentType::getHint() {
    return std::string();
}

// =================== INTEGER ARGUMENT TYPE =====================


Value & IntArg::parse(std::string s) {
    try {
        return *new Value(std::stoi(s));
    } catch (std::invalid_argument) {
        throw std::invalid_argument("Expected number not string");
    };
}

std::string IntArg::getHint() {
    return "<int>";
}

// =================== STRING ARGUMENT TYPE =====================


Value & StrArg::parse(std::string s) {
    return *new Value(s);
}

std::string StrArg::getHint() {
    return "<str>";
}

// =================== OTHER =====================


CommandLine process(int argc, char **argv) {
    CommandLine vec;
    for (int i = 1; i < argc; i++) {
        vec.emplace_back(argv[i]);
    }
    return vec;
}

// =================== VALUE =====================

Value::Value(int number) {
    this->number = number;
    is_str = false;
}

Value::Value(std::string str) {
    this->str = str;
    is_str = true;
}

Value::Value(const char * str) {
    Value(std::string(str));
}

int Value::getNumber() {
    if (is_str) {
        throw std::invalid_argument ("It is string not a number");
    }
    return number;
}

std::string Value::getString() {
    if (!is_str) {
        throw std::invalid_argument ("It is number not a string");
    }
    return str;
}

std::ostream & operator<<(std::ostream &os, Value &value) {
    if (value.is_str) {
        os << value.str;
    } else {
        os << value.number;
    }
    return os;
}

OptionValues::OptionValues(std::string name) {
    optionName = name;
}
