﻿#pragma once

#include <string>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <iostream>

typedef std::vector<std::string> CommandLine;
typedef std::vector<std::string>::iterator CommandLineIterator;

CommandLine process(int argc, char **argv);

class Value {
private:
    bool is_str;
    int number;
    std::string str;
public:
    explicit Value(int);
    explicit Value(std::string);
    explicit Value(const char*);

    friend std::ostream & operator <<(std::ostream &os, Value &value);

    int getNumber();
    std::string getString();
};

class ArgumentType {
public:
    virtual Value & parse(std::string);
    virtual std::string getHint();
};


class IntArg:public ArgumentType {
public:
    std::string getHint() override;
    Value & parse(std::string) override;
};

class StrArg:public ArgumentType {
public:
    std::string getHint() override;
    Value & parse(std::string) override;
};

class OptionValues {
public:
    explicit OptionValues(std::string);
    std::string optionName;
    std::vector<Value *> values;
};

class Option {
public:
    std::string about;
    std::string getHint();
    Option(std::string name, std::string longForm, std::string shortForm);
    void addArgument(ArgumentType *arg);
    std::vector<ArgumentType *> args; // Список типов аргументов
    std::string name;
    std::string longForm;
    std::string shortForm;
    OptionValues & parse(CommandLineIterator &it, CommandLineIterator end);
};

class OptionRuleSet {
public:
    OptionRuleSet();
    std::string programName;
    std::string about;

    std::map<std::string, Option *> forms;
    std::vector<Option *> options;

    std::string getHelp();
    std::map<std::string, OptionValues*> parse(CommandLineIterator begin, CommandLineIterator end);
    void addOption(Option *option);
};


