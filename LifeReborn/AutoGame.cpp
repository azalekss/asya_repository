#include "AutoGame.h"

AutoGame::AutoGame(GameParams &gp) {
    init(gp);
}

int AutoGame::countNeighbor(int i, int j) {
    int sumLife = 0;
    for (int x = i - 1; x <= i + 1; x++) {
        for (int y = j - 1; y <= j + 1; y++) {
            if ((x == i) && (y == j)) continue;

            sumLife += (*first)[pX(x)][pY(y)];
        }
    }
    return sumLife;
}

int AutoGame::pX(int x) {
    if (-1 == x) return this->x - 1;
    if (x == this->x) return 0;
    return x;
}

int AutoGame::pY(int y) {
    if (-1 == y) return this->y - 1;
    if (y == this->y) return 0;
    return y;
}

void AutoGame::step() {
    for (int i = 0; i < x; i++) {
        for (int j = 0; j < y; j++) {
            int neighbor = countNeighbor(i, j);
            if ((*first)[i][j]) {
                (*second)[i][j] = ((n <= neighbor) && (neighbor <= k));
            } else {
                (*second)[i][j] = (neighbor == m);
            }
        }
    }
    Field *temp = first;
    first = second;
    second = temp;
}

void AutoGame::print(std::ostream &outputFile) {
    for (int j = 0; j < y; j++) {
        for (int i = 0; i < x; i++) {
            outputFile << ((*first)[i][j] ? "*" : ".");
        }
        outputFile << std::endl;
    }
}

void AutoGame::process() {
    std::ofstream outputFile(this->outputFile);
    for (int i = 0; i < this->iterations; i++) {
        this->step();
        this->print(outputFile);
    }
    outputFile.close();
}

void AutoGame::init(GameParams &gp) {
    delete first;
    delete second;

    this->x = gp.x;
    this->y = gp.y;
    this->m = gp.m;
    this->n = gp.n;
    this->k = gp.k;
    this->iterations = gp.iterations;

    first = new Field(x, std::vector<bool>(y, false));
    second = new Field(x, std::vector<bool>(y, false));
    for (auto &i : gp.field) {
        (*first)[i.first][i.second] = true;
    }
    outputFile = gp.file;
}

//AutoGame::AutoGame() = default;

