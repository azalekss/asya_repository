#pragma once

#include <vector>
#include <iostream>
#include "GameParams.h"

typedef std::vector<std::vector<bool>> Field;

class AutoGame{
protected:
    void init(GameParams &);
public:
    Field *first = nullptr;
    Field *second = nullptr;
    int x, y, m, n, k, iterations;

    int countNeighbor(int, int);
    int pX(int);
    int pY(int);

    std::string outputFile;

    explicit AutoGame(GameParams &);

    //AutoGame();

    void step();
    void print(std::ostream &);
    void process();
};
