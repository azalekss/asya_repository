#include "FileParser.h"

FileParser::FileParser(std::ifstream &stream) {
    std::string s;
    int lineNumber = 0;

    while (!stream.eof()) {
        getline(stream, s);
        lineNumber++;
        std::stringstream ss = std::stringstream(s);
        processArgument(ss, lineNumber);
    }
}

void FileParser::processArgument(std::stringstream &s, int number) {

    std::string cmd;
    s >> cmd;

    if ("FIELD" == cmd) {
        int x, y;
        s >> x >> y;
        gp.cmdField(x, y);
    } else if ("RULES" == cmd) {
        int m, n, k;
        s >> m >> n >> k;
        gp.cmdRules(m, n, k);
    } else if ("SET" == cmd) {
        int x, y;
        s >> x >> y;
        gp.cmdSet(x, y);
    } else if (cmd.empty()) {
        return;
    } else if (cmd[0] == '#') {
        return;
    } else {
        throw std::invalid_argument("Failed command, error in " + std::to_string(number) + " string");
    }

    std::string rest;
    s >> rest;
    if (rest[0] == '#') {
        return;
    }
    if (!rest.empty()) {
        throw std::invalid_argument("Error in " + std::to_string(number) + " string");
    }
}

