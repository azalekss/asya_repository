#pragma once

#include <istream>
#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

#include "GameParams.h"

class FileParser {
public:
    explicit FileParser(std::ifstream &);
    GameParams gp;
private:
    void processArgument(std::stringstream &, int);
};
