#include "GameParams.h"

void GameParams::cmdField(int x, int y) {

    if (isFirstsField) {
        this->x = x;
        this->y = y;
        isFirstsField = false;
    } else {
        throw std::invalid_argument("Field size already set");
    }
}

void GameParams::cmdRules (int m, int n, int k) {

    if (isFirstRules) {
        this->m = m;
        this->n = n;
        this->k = k;
        isFirstRules = false;
    } else {
        throw std::invalid_argument("Rules already set");
    }
}

void GameParams::cmdSet(int x, int y) {
    if ((this->x < x) || (this->y < y) || (y < 0) || (x < 0)) {
        throw std::invalid_argument("A dot not in our field");
    }
    field.emplace_back(x, y);
}



GameParams::GameParams() : x(-1), y(-1), m(-1), n(-1), k(-1), isFirstRules(true),
                           isFirstsField(true), iterations(-1) {}

GameParams &GameParams::operator+(GameParams &right) {
    GameParams &left = *this;
    GameParams &res = *new GameParams();

    res.x = (right.x != -1) ? right.x : left.x;
    res.y = (right.y != -1) ? right.y : left.y;
    res.m = (right.m != -1) ? right.m : left.m;
    res.n = (right.n != -1) ? right.n : left.n;
    res.k = (right.k != -1) ? right.k : left.k;
    res.iterations = (right.iterations != -1) ? right.iterations : left.iterations;

    res.field.insert(res.field.end(), left.field.begin(), left.field.end());
    res.field.insert(res.field.end(), right.field.begin(), right.field.end());

    return res;
}





