#pragma once

#include <utility>
#include <vector>
#include <stdexcept>
#include <fstream>

class GameParams {
public:
    int x, y, m, n, k, iterations;
    std::string file;
    std::vector<std::pair<int, int>> field;

    explicit GameParams();
    void cmdField(int, int);
    void cmdRules(int, int, int);
    void cmdSet(int, int);

    GameParams & operator+(GameParams &right);

private:
    bool isFirstsField;
    bool isFirstRules;
};
