#include <unordered_map>
#include "InteractiveGame.h"

void InteractiveGame::reset() {
    for (int j = 0; j < y; j++) {
        for (int i = 0; i < x; i++) {
            (*first)[i][j] = false;
        }
    }
    saveState();
}

void InteractiveGame::set(int x, int y) {
    (*first)[x][y] = true;
    saveState();

}

void InteractiveGame::clear(int x, int y) {
    (*first)[x][y] = false;
    saveState();
}

void InteractiveGame::step(int n) {
    for (int i = 0; i < n; i++){
        AutoGame::step();
        saveState();
    }
}

void InteractiveGame::back() {
    if (states.size() == 1)
        return;

    states.pop_back();
    *first = states.back().field;
    x = states.back().x;
    y = states.back().y;
    m = states.back().m;
    n = states.back().n;
    k = states.back().k;
}

void InteractiveGame::save(std::ofstream & file) {
    AutoGame::print(file);
}

void InteractiveGame::load(GameParams &gp) {
    init(gp);
    saveState();
}

void InteractiveGame::rules(int m, int n, int k) {
    this->m = m;
    this->n = n;
    this->k = k;
    saveState();
}

InteractiveGame::InteractiveGame(GameParams &gp) : AutoGame(gp) {
    states.push_back(*new State{*first, x, y, m, n, k});

}

void InteractiveGame::saveState() {
    states.push_back(*new State{*first, x, y, m, n, k});
}
