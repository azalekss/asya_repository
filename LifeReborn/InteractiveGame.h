#pragma once

#include <string>
#include "GameParams.h"
#include "AutoGame.h"

typedef struct _State {
    Field field;
    int x, y, m, n, k;
} State;

class InteractiveGame: public AutoGame {
public:
    InteractiveGame(GameParams &);

    std::vector<State> states;

    void reset();
    void set(int, int);
    void clear(int x, int y);
    void step(int);
    void back();
    void save(std::ofstream &);
    void load(GameParams &);
    void rules(int, int, int);

    void saveState();
};
