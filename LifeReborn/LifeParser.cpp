#include "LifeParser.h"
#include "FileParser.h"


GameParams LifeParser::parseGameParams(CommandLineIterator begin, CommandLineIterator end) {

    std::map<std::string, OptionValues*> ovs = this->parse(begin, end);

    GameParams cmdParams = gameParamsFromCommandLine(ovs);

    try {
        auto fileName = ovs.at("input")->values[0]->getString();

        std::ifstream f(fileName);
        if (!f.is_open()) {
            throw std::invalid_argument ("File can not be open");

        }

        FileParser fp(f);
        f.close();

        cmdParams = fp.gp + cmdParams;

    } catch (std::out_of_range &ex) {}

    return cmdParams;
}

LifeParser::LifeParser() {

    this->programName = "Life_parser";
    this->about = "Parser for the game <Life>.";

    Option &input = *new Option("input", "input", "if");
    auto *strArg = new StrArg();
    input.addArgument(strArg);
    input.about = "Input file name";

    this->addOption(&input);

    Option &output = *new Option("output", "output", "o");
    auto *intArg = new IntArg();
    output.addArgument(intArg);
    output.about = "Output file name";

    this->addOption(&output);

    Option &iterations = *new Option("iterations", "iterations", "ic");
    auto *iterationsArg = new IntArg();
    output.addArgument(iterationsArg);
    output.about = "Count of iterations";

    this->addOption(&iterations);

    Option &field = *new Option("field", "field", "f");
    auto *xArg = new IntArg();
    auto *yArg = new IntArg();
    field.addArgument(xArg);
    field.addArgument(yArg);
    field.about = "Set size of field";

    this->addOption(&field);

    Option &help = *new Option("help", "help", "h");
    output.about = "Help";

    this->addOption(&help);

    Option &m = *new Option("m", "M", "m");
    auto *mArg = new IntArg();
    m.addArgument(mArg);
    m.about = "The number of neighbors at which life in the next course of life";

    this ->addOption(&m);

    Option &n = *new Option("n", "N", "n");
    auto *nArg = new IntArg();
    n.addArgument(nArg);
    n.about = "The minimum number of neighbors to survive";

    this ->addOption(&n);

    Option &k = *new Option("k", "K", "k");
    auto *kArg = new IntArg();
    k.addArgument(kArg);
    k.about = "The maximum number of neighbors to survive";

    this ->addOption(&k);
}

GameParams LifeParser::gameParamsFromCommandLine(mapOvs ovs) {

    GameParams gameParamsComLine;

    try {
        std::vector<Value *> &values = ovs.at("field")->values;
        gameParamsComLine.cmdField(values[0]->getNumber(), values[1]->getNumber());
    } catch (std::out_of_range &ex) {}

    try {
        int iter = ovs.at("iterations")->values[0]->getNumber();
        gameParamsComLine.iterations = iter;
    } catch (std::out_of_range &ex) {}

    try {
        auto fileName = ovs.at("output")->values[0]->getString();
        gameParamsComLine.file = fileName;
    } catch (std::out_of_range &ex) {}

    int m = -1, n = -1, k = -1;
    try {
        m = ovs.at("m")->values[0]->getNumber();
    } catch (std::out_of_range &ex) {}

    try {
        n = ovs.at("n")->values[0]->getNumber();
    } catch (std::out_of_range &ex) {}

    try {
        k = ovs.at("k")->values[0]->getNumber();
    } catch (std::out_of_range &ex) {}

    gameParamsComLine.cmdRules(m, n, k);

    return gameParamsComLine;
}
