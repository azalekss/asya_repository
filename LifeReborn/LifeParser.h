#pragma once

#include "ArgParser.h"
#include "GameParams.h"
#include <iostream>
#include <fstream>

typedef std::map<std::string, OptionValues*> mapOvs;

class LifeParser : public OptionRuleSet {
public:
    LifeParser();
    GameParams parseGameParams(CommandLineIterator begin, CommandLineIterator end);

private:
    GameParams gameParamsFromCommandLine(mapOvs);
};
