#include <iostream>
#include <vector>
#include "ArgParser.h"
#include "FileParser.h"
#include "LifeParser.h"
#include "AutoGame.h"
#include "InteractiveGame.h"

int main(int argc, char *argv[]) {

    GameParams defaultGameParams;
    defaultGameParams.x = 10;
    defaultGameParams.y = 10;
    defaultGameParams.m = 3;
    defaultGameParams.n = 2;
    defaultGameParams.k = 3;
    defaultGameParams.iterations = 10;

    CommandLine vec = process(argc, argv);

    LifeParser &lifeParser = *new LifeParser();
    GameParams gp = lifeParser.parseGameParams(vec.begin(), vec.end());
    gp = defaultGameParams + gp;

    if (argc == 1) {
        InteractiveGame interactiveGame(gp);
        while (true) {
            std::string cmd;
            std::cin >> cmd;
            if (cmd == "reset") interactiveGame.reset();
            else if (cmd == "clear") {
                int x, y;
                std::cin >> x >> y;
                interactiveGame.clear(x, y);
            } else if (cmd == "set") {
                int x, y;
                std::cin >> x >> y;
                interactiveGame.set(x, y);
            } else if (cmd == "step") {
                int n;
                std::cin >> n;

                interactiveGame.step(n);

            } else if (cmd == "back") interactiveGame.back();
            else if (cmd == "save") {
                std::string fileName;
                std::cin >> fileName;
                std::ofstream file(fileName);
                interactiveGame.save(file);
                file.close();
            } else if (cmd == "load") {
                std::string fileName;
                std::cin >> fileName;
                std::ifstream file(fileName);
                FileParser fileParser(file);
                interactiveGame.load(defaultGameParams + fileParser.gp);
                file.close();
            } else if (cmd == "rules") {
                int m, n, k;
                std::cin >> m >> n >> k;
                interactiveGame.rules(m, n, k);
            } else if (cmd == "exit") break;
            else continue;

            interactiveGame.print(std::cout);

        }
    }
    else {
        AutoGame game(gp);
        game.process();
    }
}