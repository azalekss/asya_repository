#include <cstdlib>
#include "tritset.h"

int main(int argc, char** argv){
    /*using namespace myPersonalContainer;

    TritSet a;
    a[0] = TritSet::Trit::TRUE;
    a[1] = TritSet::Trit::FALSE;
    std::cout<<a[0]<<a[1]<<a[2];

    //TritSet b(-4);

    a[50] = TritSet::Trit::FALSE;
    TritSet b;
    TritSet c;
    c[19] = TritSet::Trit::TRUE;
    std::cout <<(a.capacity() == 16)<<' '<<(b.capacity() == 0) <<' '<<(c.capacity() == 8)<<"\n";

    TritSet d;
    d[10] = TritSet::Trit::TRUE;
    d[20] = TritSet::Trit::TRUE;
    std::cout<<(d.length() == 21)<<' ';
    d[20] = TritSet::Trit::UNKNOWN;
    d.shrink();
    std::cout<<(d.length() == 11)<<' '<<d[10]<<d[11]<<d[12]<<'\n';

    TritSet e;
    e[0] = TritSet::Trit::TRUE;
    e[1] = TritSet::Trit::FALSE;
    e[20] = TritSet::Trit::FALSE;
    e[101] = TritSet::Trit::FALSE;
    std::cout<<(e.cardinality(TritSet::Trit::FALSE) == 3)<<' '<<(e.cardinality(TritSet::Trit::TRUE) == 1)<<' '<<(e.cardinality(TritSet::Trit::UNKNOWN) == 97)<<'\n';
    e[101] = TritSet::Trit::UNKNOWN;
    e[20] = TritSet::Trit::UNKNOWN;
    e[21] = TritSet::Trit::TRUE;
    std::cout<<e[0]<<e[1]<<e[20]<<e[21]<<e[101]<<' ';
    std::cout<<(e.cardinality(TritSet::Trit::FALSE) == 1)<<' '<<(e.cardinality(TritSet::Trit::TRUE) == 2)<<' '<<(e.cardinality(TritSet::Trit::UNKNOWN) == 18)<<'\n';

    TritSet f;
    f[0] = TritSet::Trit::TRUE;
    f[11] = TritSet::Trit::TRUE;
    f[12] = TritSet::Trit::FALSE;
    f[13] = TritSet::Trit::FALSE;
    f.trim(12);
    std::cout<<(f[12] == TritSet::Trit::UNKNOWN)<<' '<<(f[12] == TritSet::Trit::UNKNOWN)<< '\n';
    //f.trim(-1);

    TritSet g, h;
    g[0] = TritSet::Trit::TRUE;
    g[1] = TritSet::Trit::FALSE;
    g[3] = TritSet::Trit::TRUE;

    h[0] = TritSet::Trit::FALSE;
    h[1] = TritSet::Trit::TRUE;
    h[2] = TritSet::Trit::TRUE;

    TritSet i = g & h;
    TritSet j = g | h;
    TritSet k = !g;
    std::cout<<g[0]<<g[1]<<g[2]<<g[3]<<' '<<h[0]<<h[1]<<h[2]<<h[3]<<' '<<i[0]<<i[1]<<i[2]<<i[3]<<' '<<j[0]<<j[1]<<j[2]<<j[3]<<' '<<k[0]<<k[1]<<k[2]<<k[3]<<'\n';

    TritSet l;
    l[0] = TritSet::Trit::TRUE;
    l[1] = TritSet::Trit::TRUE;
    l[2] = TritSet::Trit::TRUE;
    l[3] = TritSet::Trit::TRUE;
    l[90] = TritSet::Trit::TRUE;
    //TritSet i = l & g;
    //TritSet j = l | g;
    //TritSet k = !l;

    std::cout<<l[0]<<l[1]<<l[2]<<l[3]<<' '<<i[0]<<i[1]<<i[2]<<i[3]<<' '<<j[0]<<j[1]<<j[2]<<j[3]<<' '<<k[0]<<k[1]<<k[2]<<k[3]<<'\n';

    TritSet m;
    std::istream &stream = std::cin;
    m.loadFromStream(stream);

    std::ostream &stream2 = std::cout;
    m.saveToStream(stream2);
    l.saveToStream(stream2);

    system("pause");/*/

    return 0;
}
