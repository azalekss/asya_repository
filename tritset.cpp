#include <stdexcept>
#include "tritset.h"

namespace myPersonalContainer{

    //------------------------------------------------------------------------------------- constructors

    TritSet::TritSet() : myArr(0), size(0){
        changeSize (0);
        this -> trueTrit = 0;
        this -> falseTrit = 0;
        this -> lastNotUnknown = -1;
    }

    TritSet::TritSet(int requiredSize) : myArr(0), size(0){
        if (requiredSize < 0){
            throw std::runtime_error("size < 0");
        }
        this -> trueTrit = 0;
        this -> falseTrit = 0;
        this -> lastNotUnknown = -1;
        changeSize(requiredSize);
    }

    TritSet::TritSet(const TritSet& other) : myArr(0), size(0){
        this -> trueTrit = other.trueTrit;
        this -> falseTrit = other.falseTrit;
        this -> lastNotUnknown = other.lastNotUnknown;
        this -> changeSize(other.size);
        int i;
        for (i = 0; i < (this -> size + NUM_OF_2BIT - 1) / NUM_OF_2BIT; i++){
            this -> myArr[i] = other.myArr[i];
        }
    }

    //-------------------------------------------------------------------------------------- operators

    TritSet& TritSet::operator=(const TritSet& other){
        this -> changeSize(other.size);
        this -> trueTrit = other.trueTrit;
        this -> falseTrit = other.falseTrit;
        this -> lastNotUnknown = other.lastNotUnknown;
        delete[] this -> myArr;
        int i;
        for (i = 0; i < (this -> size + NUM_OF_2BIT - 1) / NUM_OF_2BIT; i++){
            this -> myArr[i] = other.myArr[i];
        }
        return *this;
    }

    const TritSet::Trit TritSet::operator[](int index) const{
        return get(index);
    }

    TritHolder TritSet::operator[](int index){
        return TritHolder(*this, index);
    }

    TritSet TritSet::operator&(TritSet other){
        return this -> andOrNot(TritSet::AND_OPER, other);
    }

    TritSet TritSet::operator|(TritSet other){
        return this -> andOrNot(TritSet::OR_OPER, other);
    }

    TritSet TritSet::operator!(){
        return this -> andOrNot(TritSet::NOT_OPER, *this);
    }

    //------------------------------------------------------------------------------------ methods

    //internal array size in bytes
    int TritSet::capacity(){
        return (this -> size + NUM_OF_2BIT - 1) / NUM_OF_2BIT * SIZE_UINT;
    }

    //cutting the array to the value required to store the last set trit
    void TritSet::shrink(){
        changeSize(length());
    }

    //the number of trits of a given type
    int TritSet::cardinality(Trit value){
        switch (value){
            case TRUE:{
                return quantityOfTrue();
            }
            case FALSE:{
                return quantityOfFalse();
            }
            default:{
                return quantityOfUnknown();
            }
        }
    }

    //number of trits of all
    TritSet::map TritSet::cardinality(){
        map map({
            {Trit::TRUE, this -> cardinality(Trit::TRUE)},
            {Trit::FALSE, this -> cardinality(Trit::FALSE)},
            {Trit::UNKNOWN, this -> cardinality(Trit::UNKNOWN)}
        });
        return map;
    };

    //crop to the specified index
    void TritSet::trim(int lastIndex){
        int i;
        for (i = lastIndex; i < this -> length(); i++)
            (*this)[i] = UNKNOWN;

    }

    //the index of the latter is not unknown trit + 1
    int TritSet::length(){
        return this -> lastNotUnknown + 1;
    }

    TritSet::TritIterator TritSet::begin(){
        return TritIterator(*this, 0);
    };

    TritSet::TritIterator TritSet::end(){
        return TritIterator(*this, this -> length());
    };

    //--------------------------------------------------------------------------- private methods
    //put element in the required position
    void TritSet::set(int position, TritSet::Trit element){
        changeLastIndex(position, element);
        uint uns = this -> myArr[position / NUM_OF_2BIT];
        int shift = BITS_IN_TRIT * (NUM_OF_2BIT - (position % NUM_OF_2BIT) - 1);
        uint uns2 = uns >> shift;
        uint value = uns2 % 4;
        value <<= shift;
        uns -= value;
        int el = fromTrit(element);
        el <<= shift;
        this -> myArr[position / NUM_OF_2BIT] = el + uns;
    }

    void TritSet::conversion(TritSet::Trit el, int position){
        if (el == TritSet::Trit::FALSE) {
            this -> falseTrit++;
            if (get(position) == TritSet::Trit::TRUE){
                trueTrit--;
            }
        }
        if (el == TritSet::Trit::TRUE){
            this -> trueTrit++;
            if (get(position) == TritSet::Trit::FALSE){
                falseTrit--;
            }
        }
    }

    void TritSet::changeLastIndex(int position, TritSet::Trit el){
        if (position < 0) {
            throw std::runtime_error("position < 0");
        }
        if (el == TritSet::Trit::UNKNOWN){
            if (get(position) == TritSet::Trit::FALSE){
                falseTrit--;
            }
            if (get(position) == TritSet::Trit::TRUE){
                trueTrit--;
            }
            if (lastNotUnknown == position){
                int i = lastNotUnknown - 1;
                while ((get(i) == TritSet::Trit::UNKNOWN) && (i >= 0)){
                    i--;
                }
                lastNotUnknown = i;
            }
            if (lastNotUnknown < position){
                return;
            }
        }
        else{
            if (position >= size){
                changeSize(position + 1);
                lastNotUnknown = position;
            }
            if (position > lastNotUnknown){
                lastNotUnknown = position;
            }
            conversion(el, position);
        }
    }

    //take the trit from the index
    const TritSet::Trit TritSet::get(int index) const{
        if (index < 0){
            throw std::runtime_error("index < 0");
        }
        if (index >= this -> size){
            return UNKNOWN;
        }
        uint uns = this -> myArr [index / NUM_OF_2BIT];
        uns >>= BITS_IN_TRIT * (NUM_OF_2BIT - (index % NUM_OF_2BIT) - 1);
        int value = uns % 4;
        return fromValue(value);
    }

    void TritSet::changeSize(int count){
        int i;
        auto *newArr = new uint [(count + NUM_OF_2BIT - 1) / NUM_OF_2BIT]();
        int min = (count + NUM_OF_2BIT - 1) / NUM_OF_2BIT;
        if (this -> size < count)
            min = (this -> size + NUM_OF_2BIT - 1) / NUM_OF_2BIT;
        for (i = 0; i < min; i++)
            newArr[i] = myArr[i];
        this -> size = count;
        if (this -> myArr != 0){
            delete[] this -> myArr;
        }
        this -> myArr = newArr;
    }

    //get a trit on the corresponding int value
    TritSet::Trit TritSet::fromValue(int value) const{
        switch (value){
            case 1:{
                return TRUE;
            }
            case 2:{
                return FALSE;
            }
            default:{
                return UNKNOWN;
            }
        }
    }

    //get a int value on the corresponding trit
    int TritSet::fromTrit(TritSet::Trit trit){
        switch (trit){
            case TRUE:{
                return 1;
            }
            case FALSE:{
                return 2;
            }
            default:{
                return 0;
            }
        }
    }

    int TritSet::quantityOfTrue(){
        return trueTrit;
    }

    int TritSet::quantityOfFalse(){
        return falseTrit;
    }

    int TritSet::quantityOfUnknown(){
        return this -> lastNotUnknown - trueTrit - falseTrit;
    }

    //-------------------------------------------------------------------------------- logic operation

    TritSet TritSet::andOrNot(int parameter, TritSet other){
        int max = this -> length();
        if (this -> length() < other.length()){
            max = other.length();
        }
        TritSet result(max);
        TritSet &myTritSet = *this;

        switch (parameter){
            case (TritSet::AND_OPER):{
                for (int i = 0; i < max; i++){
                    result[i] = andOper(other[i], myTritSet[i]);
                }
                break;
            }
            case (TritSet::OR_OPER):{
                for (int i = 0; i < max; i++){
                    result[i] = orOper(other[i], myTritSet[i]);
                }
                break;
            }
            case (TritSet::NOT_OPER):{
                for (int i = 0; i < this -> size; i++){
                    result[i] = notOper(myTritSet[i]);
                }
                break;
            }
            default:{
                break;
            }
        }
        return result;
    }

    TritSet::Trit TritSet::andOper(Trit first, Trit second){
        if ((first == Trit::FALSE) || (second == Trit::FALSE)){
            return Trit::FALSE;
        }
        else{
            if ((first == Trit::TRUE) && (second == Trit::TRUE)){
                return Trit::TRUE;
            }
            else{
                return Trit::UNKNOWN;
            }
        }
    }

    TritSet::Trit TritSet::orOper(Trit first, Trit second){
        if ((first == Trit::TRUE) || (second == Trit::TRUE)){
            return Trit::TRUE;
        }
        else{
            if ((first == Trit::FALSE) && (second == Trit::FALSE)){
                return Trit::FALSE;
            }
            else{
                return Trit::UNKNOWN;
            }
        }
    }

    TritSet::Trit TritSet::notOper(Trit trit){
        if (trit == Trit::TRUE){
            return Trit::FALSE;
        }
        else{
            if (trit == Trit::FALSE){
                return Trit::TRUE;
            }
            else{
                return Trit::UNKNOWN;
            }
        }
    }

    // !!additional task!!
    void TritSet::saveToStream(std::ostream& stream){
        std::cout << magic_constant << "|" << ((this -> size + NUM_OF_2BIT - 1) / NUM_OF_2BIT) << "|";
        int i;
        for (i = 0; i < ((this -> size + NUM_OF_2BIT - 1) / NUM_OF_2BIT); i++){
            stream << myArr[i] << " ";
        }
    }

    void TritSet::loadFromStream(std::istream &stream){
        int magic;
        stream >> magic;
        if (magic != magic_constant){
            throw std::domain_error("no TritSet");
        }
        stream.ignore (1, '|');
        int arrSize = -1;
        stream >> arrSize;
        this -> size = arrSize * SIZE_UINT * BITS_IN_BYTE / BITS_IN_TRIT;
        this -> changeSize(this -> size);
        stream.ignore (1, '|');
        int i;
        for (i = 0; i < arrSize; i++){
            stream >> myArr[i];
        }
    }

    //----------------------------------------------------------------------------------------- Holder
    //constructor
    TritHolder::TritHolder(TritSet& set, int index) : set(set), index(index){};
    //operators
    TritSet::Trit TritHolder::operator|(TritSet::Trit trit){
        return this -> set.orOper(*this, trit);
    }

    TritSet::Trit TritHolder::operator&(TritSet::Trit trit){
        return this -> set.andOper(*this, trit);
    }

    TritSet::Trit TritHolder::operator~(){
        return this -> set.notOper(*this);
    }

    TritHolder& TritHolder::operator=(TritSet::Trit el){
        this -> set.set(this -> index, el);
        return *this;
    }

    TritHolder& TritHolder::operator=(const TritHolder &other){
        this -> set.set(this -> index, other.set.get(other.index));
        return *this;
    }

    TritHolder::operator TritSet::Trit(){
        return this -> set.get(this -> index);
    }

    //---------------------------------------------------------------------------------------- Iterator
    //constructor
    TritSet::TritIterator::TritIterator(TritSet& set, int index) : set(set), index(index){};
    //operators
    TritHolder TritSet::TritIterator::operator*(){
        return this -> set[this -> index];
    }

    TritSet::TritIterator& TritSet::TritIterator::operator++(){
        this -> index++;
        return *this;
    }

    TritSet::TritIterator& TritSet::TritIterator::operator++(int){
        this -> index++;
        return *this;
    }

    bool TritSet::TritIterator::operator==(TritIterator other){
        return this -> index == other.index;
    }

    bool TritSet::TritIterator::operator!=(TritIterator other){
        return this -> index != other.index;
    }

    TritSet::TritHash::TritHash() = default;;

    int TritSet::TritHash::operator()(const Trit& el) const{
        switch(el){
            case Trit::TRUE:{
                return 1;
            }
            case Trit::FALSE:{
                return 2;
            }
            default:{
                return 0;
            }
        }
    }
};
