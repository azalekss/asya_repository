#ifndef TRY_2_TRITSET_H
#define TRY_2_TRITSET_H

#include <iostream>
#include <unordered_map>

namespace myPersonalContainer{

    class TritHolder;
    class TritSet{

        public:

            class TritIterator;
            class TritHash;

            enum Trit{
                FALSE = -1,
                UNKNOWN = 0,
                TRUE = 1,
            };

            //types
            typedef unsigned int uint;
            typedef std::unordered_map <Trit, int, TritHash> map;

            //constructors
            TritSet();
            TritSet(int requiredSize);
            TritSet(const TritSet &other);

            //operators
            TritSet &operator=(const TritSet &other);
            const Trit operator[](int index) const;
            TritHolder operator[](int index);
            TritSet operator&(TritSet other);
            TritSet operator|(TritSet other);
            TritSet operator!();

            //methods
            int capacity();
            void shrink();
            int cardinality (Trit value);
            map cardinality();
            void trim (int lastIndex);
            int length();

            TritIterator begin();
            TritIterator end();

            void loadFromStream(std::istream &stream);
            void saveToStream(std::ostream& stream);

        private:
            //friends
            friend class TritHolder;

            //constants
            static const int SIZE_UINT = sizeof(uint);
            static const int BITS_IN_BYTE = 8;
            static const int BITS_IN_TRIT = 2;
            static const int NUM_OF_2BIT = SIZE_UINT * BITS_IN_BYTE / BITS_IN_TRIT; //16
            static const int AND_OPER = 1;
            static const int OR_OPER = 2;
            static const int NOT_OPER = 3;
            static const int magic_constant = 505;

            uint* myArr;
            int size;
            int trueTrit;
            int falseTrit;
            int lastNotUnknown;

            //methods
            void set(int position, Trit element);
            void conversion(TritSet::Trit el, int position);
            void changeLastIndex(int position, TritSet::Trit el);
            const Trit get(int position) const;

            void changeSize(int count);

            Trit fromValue(int value) const;
            int fromTrit(Trit trit);

            int quantityOfTrue();
            int quantityOfFalse();
            int quantityOfUnknown();

            TritSet andOrNot(int parameter, TritSet other);

            Trit andOper(Trit first, Trit second);
            Trit orOper(Trit first, Trit second);
            Trit notOper(Trit trit);
    };

        class TritHolder{
            public:
                TritHolder(TritSet& set, int index);
                TritSet::Trit operator|(TritSet::Trit trit);
                TritSet::Trit operator&(TritSet::Trit trit);
                TritSet::Trit operator~();
                TritHolder& operator=(TritSet::Trit el);
                TritHolder& operator=(const TritHolder &other);
                operator TritSet::Trit();

            private:
                TritSet& set;
                int index;
        };

        class TritSet::TritIterator{
            public:
                TritIterator(TritSet& set, int index);
                TritHolder operator*();
                TritIterator& operator++();
                TritIterator& operator++(int);
                bool operator==(TritIterator other);
                bool operator!=(TritIterator other);

            private:
                TritSet& set;
                int index;
        };

        class TritSet::TritHash{
            public:
                TritHash();
                int operator()(const Trit&) const;
        };
};

#endif //TRY_2_TRITSET_H